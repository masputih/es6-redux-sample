//konstanta buat Action type biar nggak salah ketik
export const SET_SOURCE = 'SET_SOURCE';
export const PLAY = 'PLAY';
export const PAUSE = 'PAUSE'
export const SEEK = 'SEEK';
export const VOLUME_UP = 'VOLUME_UP';
export const VOLUME_DOWN = 'VOLUME_DOWN';
export const VIDEO_STATUS_CHANGED = 'VIDEO_STATUS_CHANGED'
export const UPDATE_TIME = 'UPDATE_TIME';

export const setSource = (url) =>({
  type:SET_SOURCE,
  url
});

export const play = () =>({
  type:PLAY
});

export const pause = () =>({
  type:PAUSE
});

export const seek = (time) =>({
  type:SEEK,
  time
});

export const volumeUp = () =>({
  type:VOLUME_UP
});

export const volumeDown = () =>({
  type:VOLUME_DOWN
});

export const videoStatusChanged = ( status )=>({
  type: VIDEO_STATUS_CHANGED,
  status
});

export const updateTime = ({time,duration})=>({
  type: UPDATE_TIME,
  time,
  duration
})