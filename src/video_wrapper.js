import * as Status from './status';
import { videoStatusChanged, updateTime } from './actions';

const VideoWrapper = (store) => {
  const videoEl = document.getElementById('myvideo');
  videoEl.addEventListener('playing', (e) => {
    store.dispatch(videoStatusChanged(Status.PLAYING));
  });

  videoEl.addEventListener('pause', (e) => {
    store.dispatch(videoStatusChanged(Status.PAUSED));
  });

  videoEl.addEventListener('timeupdate',()=>{
    //tampilin 2 desimal aja
    const time = parseFloat(videoEl.currentTime.toFixed(2));
    const duration = parseFloat(videoEl.duration.toFixed(2));

    store.dispatch(updateTime({
      time:time,
      duration:duration
    }));
  });

  store.subscribe(() => {
    let state = store.getState();
    if (state.status === Status.NEW_SOURCE) {
      videoEl.src = state.source;
    } else if (state.status === Status.PLAY_REQUESTED) {
      if (videoEl.src) {
        videoEl.play();
      }
    } else if(state.status === Status.PAUSE_REQUESTED){
      if(videoEl.src) {
        videoEl.pause();
      }
    } else if(state.status === Status.SEEK_REQUESTED){
      if(videoEl.src){
        videoEl.currentTime = state.time;
      }
    }

  });

}

export default VideoWrapper; 