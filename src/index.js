import './styles.scss';

import { createStore } from 'redux';
import * as Actions from './actions';
import * as Status from './status';
import VideoWrapper from './video_wrapper';
import SourceInput from './ui/source-input';
import PlayPauseBtn from './ui/playpause-btn';
import TimeUI from './ui/time-ui';

const defaultVideoState = {
  source: undefined,
  status: undefined,
  volume: 1,
  duration: 0,
  time: 0
};

const videoStateReducer = (state = defaultVideoState, action) => {
  console.log('VideoStateReducer','state',state,'action', action);
  switch(action.type){
    case Actions.SET_SOURCE:
      return {
        ...state,
        status: Status.NEW_SOURCE,
        source: action.url
      }
    case Actions.PLAY:
      return {
        ...state,
        status: Status.PLAY_REQUESTED
      }
    case Actions.PAUSE:
      return {
        ...state,
        status: Status.PAUSE_REQUESTED
      }
    case Actions.VIDEO_STATUS_CHANGED:
      return{
        ...state,
        status: action.status
      }
    case Actions.UPDATE_TIME:
      return {
        ...state,
        time: action.time,
        duration: action.duration
      }
    case Actions.SEEK:
      return{
        ...state,
        status: Status.SEEK_REQUESTED,
        time: action.time
      }
  }
  return state;
};

const videoStore = createStore(videoStateReducer);
window.videoStore = videoStore;

videoStore.subscribe((state)=>{
  console.log('VideoStore:current state', videoStore.getState());
});


//init videowrapper
VideoWrapper(videoStore);
SourceInput(videoStore);
PlayPauseBtn(videoStore);
TimeUI(videoStore);