import * as Status from '../status';
import { seek } from '../actions';

const TimeUI = (store) => {

  let isFocused = false;
  let valueBeforeFocused = undefined;

  const timeInput = document.querySelector('input[name=time]');
  timeInput.addEventListener('focusin', () => {
    isFocused = true;
    valueBeforeFocused = timeInput.value;
  });

  timeInput.addEventListener('focusout', () => {
    isFocused = false;
    if (timeInput.value != valueBeforeFocused) {
      store.dispatch(seek(timeInput.value));
    }
  });

  const duration = document.getElementById('duration');

  store.subscribe(() => {
    const state = store.getState();
    if (!isFocused) {
      timeInput.value = state.time;
    }
    duration.innerHTML = state.duration;
  })
};

export default TimeUI;