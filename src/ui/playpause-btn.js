import * as Status from '../status';
import { play, pause } from '../actions';

const PlayPauseBtn = (store) => {

  const btn = document.getElementById('play-pause-btn');
  btn.addEventListener('click', (e) => {
    e.preventDefault();
    const status = store.getState().status;
    if (status === Status.PLAYING) {
      store.dispatch(pause());
    } else {
      store.dispatch(play());
    }
  })

  //update label
  store.subscribe(() => {
    const state = store.getState();
    if (state.status === Status.PLAYING) {
      btn.innerHTML = 'Pause';
    } else if (state.status === Status.PAUSED){
      btn.innerHTML = 'Play';
    }
  })
};
export default PlayPauseBtn;