import { setSource } from '../actions';

const SourceInput = (store)=>{
  
  const input = document.querySelector('input[name=video-source]');
  input.setAttribute('value','//vjs.zencdn.net/v/oceans.mp4');

  const btn = document.getElementById('load-src-btn');
  btn.addEventListener('click', (e)=>{
    e.preventDefault();
    if(input.value){
      store.dispatch(setSource(input.value));
    }
  });
}

export default SourceInput;