export const NEW_SOURCE = 'new_source';
export const PLAY_REQUESTED = 'play_requested';
export const PLAYING = 'playing';
export const PAUSE_REQUESTED = 'pause_requested';
export const PAUSED = 'paused';
export const SEEK_REQUESTED = 'seek_requested';